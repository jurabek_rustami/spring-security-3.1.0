package com.example.spring_security_1.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public User findById(Long id) {
        return userRepo.findByIdAndDeletedFalse(id);
    }

    public User findByPhone(String phone) {
        return userRepo.findByPhoneAndDeletedFalse(phone);
    }

    public User save(User user) {
        return userRepo.save(user);
    }

    public boolean delete(Long id) {
        return userRepo.delete(id);
    }

    public List<User> findAll() {
        return userRepo.findAllByDeletedFalse();
    }
}
