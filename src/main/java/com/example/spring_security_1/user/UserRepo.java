package com.example.spring_security_1.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
    List<User> findAllByDeletedFalse();

    User findByIdAndDeletedFalse(Long id);

    User findByPhoneAndDeletedFalse(String phone);

    @Query(value = "UPDATE users SET deleted = true WHERE id = ?1", nativeQuery = true)
    boolean delete(Long id);

}
