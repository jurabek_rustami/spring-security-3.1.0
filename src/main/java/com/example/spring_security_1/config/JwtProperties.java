package com.example.spring_security_1.config;

import lombok.Data;
import org.hibernate.annotations.Comment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
public class JwtProperties {
    @Value("${security.jwt.secret}")
    private String secret;
    @Value("${security.jwt.access}")
    private long access;
    @Value("${security.jwt.refresh}")
    private long refresh;

}
