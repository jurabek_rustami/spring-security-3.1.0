package com.example.spring_security_1.food;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FoodService {

    private final FoodRepo foodRepo;

    public FoodService(FoodRepo foodRepo) {
        this.foodRepo = foodRepo;
    }

    public Food findById(Long id) {
        return foodRepo.findByIdAndDeletedFalse(id);
    }

    public Food save(Food food) {
        return foodRepo.save(food);
    }

    public boolean delete(Long id) {
        return foodRepo.delete(id);
    }


    public List<Food> findAll() {
        return foodRepo.findAllByDeletedFalse();
    }

    public List<Food> findActiveFoods(List<Long> productIds) {
        String ids = String.join(",", productIds.toString().replace("[", "").replace("]", ""));
        return foodRepo.findActiveFoods(ids);
    }
}
