package com.example.spring_security_1.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LangModel {
    private String uz;
    private String ru;
    private String en;
}
