package com.example.spring_security_1.admin.auth;

import com.example.spring_security_1.admin.Admin;
import com.example.spring_security_1.admin.AdminService;
import com.example.spring_security_1.config.JwtTokenProvider;
import com.example.spring_security_1.utils.Role;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final AdminService adminService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody Admin admin) {
        String phone = admin.getPhone();
        String password = admin.getPassword();
        logger.info("phone {}, password {}", phone, password);
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(phone, password));
        Admin adminFromDb = adminService.findByPhone(phone);
        String token = jwtTokenProvider.createAdminToken(adminFromDb.getId(), phone, adminFromDb.getRoles());
        Map<Object, Object> response = Map.of("phone", phone, "token", token);
        return ResponseEntity.ok(response);
    }
}
