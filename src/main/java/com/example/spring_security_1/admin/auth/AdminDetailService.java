package com.example.spring_security_1.admin.auth;

import com.example.spring_security_1.admin.Admin;
import com.example.spring_security_1.admin.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("adminDetailService")
@RequiredArgsConstructor
public class AdminDetailService implements UserDetailsService {

    private final AdminService adminService;
    @Override
    public UserDetails loadUserByUsername(String phone) throws UsernameNotFoundException {
        Admin admin = adminService.findByPhone(phone);
        return admin;
    }
}
