package com.example.spring_security_1.order;

import java.util.EnumSet;

public enum OrderType {
    DELIVERY,
    PICKUP,
    TABLE;

    public static boolean contains(OrderType orderType) {
        return EnumSet.allOf(OrderType.class).contains(orderType);
    }
}
