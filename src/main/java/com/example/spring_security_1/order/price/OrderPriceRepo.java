package com.example.spring_security_1.order.price;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderPriceRepo extends JpaRepository<OrderPrice, Long> {
}
