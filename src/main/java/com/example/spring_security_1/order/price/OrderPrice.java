package com.example.spring_security_1.order.price;

import com.example.spring_security_1.order.Order;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders_price")
public class OrderPrice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(mappedBy = "price")
    private Order order;
    private Long foodPrice;
    private Long deliveryPrice;
    private Long totalPrice;

    public void sumPrice(Long price){
        this.totalPrice += price;
    }
}