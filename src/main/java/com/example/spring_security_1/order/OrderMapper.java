package com.example.spring_security_1.order;

import com.example.spring_security_1.address.Address;
import com.example.spring_security_1.order.address.OrderAddress;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderMapper {
    @SneakyThrows
    public static OrderDto toDto(Order order) {
        List<CartItem> foods = new ObjectMapper().readValue(order.getFoods(), new TypeReference<>() {});

        return OrderDto.builder()
                .id(order.getId())
                .user(order.getUser())
                .type(order.getType())
                .foods(foods)
                .address(Address.builder()
                                .id(order.getAddress().getId())
                                .description(order.getAddress().getDescription())
                                .latitude(order.getAddress().getLatitude())
                                .longitude(order.getAddress().getLongitude())
                                .build()).build();
    }

    @SneakyThrows
    public static Order toEntity(OrderDto orderDto) {
        String foods = new ObjectMapper().writeValueAsString(orderDto.getFoods());
        return Order.builder()
                .id(orderDto.getId())
                .userPhone(orderDto.getUser().getPhone())
                .user(orderDto.getUser())
                .type(orderDto.getType())
                .foods(foods)
                .address(OrderAddress.builder()
                        .id(orderDto.getAddress().getId())
                        .description(orderDto.getAddress().getDescription())
                        .latitude(orderDto.getAddress().getLatitude())
                        .longitude(orderDto.getAddress().getLongitude())
                        .build()).build();
    }
}
