package com.example.spring_security_1.order;

import com.example.spring_security_1.food.Food;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartItem implements Comparable<CartItem> {
    private Food food;
    private Integer count;

    @Override
    public int compareTo(CartItem cartItem) {
        return this.food.getId().compareTo(cartItem.getFood().getId());
    }
}
