package com.example.spring_security_1.order;

import com.example.spring_security_1.address.Address;
import com.example.spring_security_1.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDto {
    private Long id;
    private User user;
    private String type;
    private List<CartItem> foods;
    private Address address;

    public OrderType type() {
        return OrderType.valueOf(type);
    }
}
