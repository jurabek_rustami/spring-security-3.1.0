package com.example.spring_security_1.order;

import com.example.spring_security_1.address.Address;
import com.example.spring_security_1.address.AddressService;
import com.example.spring_security_1.food.Food;
import com.example.spring_security_1.food.FoodService;
import com.example.spring_security_1.order.address.OrderAddress;
import com.example.spring_security_1.order.address.OrderAddressRepo;
import com.example.spring_security_1.order.price.OrderPrice;
import com.example.spring_security_1.order.price.OrderPriceRepo;
import com.example.spring_security_1.user.User;
import com.example.spring_security_1.user.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final Logger logger = LoggerFactory.getLogger(OrderService.class);

    private final OrderRepo orderRepo;
    private final FoodService foodService;
    private final AddressService addressService;
    private final OrderMapper orderMapper;
    private final OrderAddressRepo orderAddressRepo;
    private final OrderPriceRepo orderPriceRepo;
    private final UserService userService;

    public ResponseEntity<?> createOrder(OrderDto orderDto) {

        if (orderDto.getFoods()==null) {
            logger.info("Food list is null");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Collections.sort(orderDto.getFoods());
        logger.info("Sorted foods: {}", orderDto.getFoods());

        List<Food> sortedFoods = orderDto.getFoods().stream().map(CartItem::getFood).toList();

        if (sortedFoods.isEmpty()) {
            logger.info("Food list is empty");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (orderDto.getUser().getId()==null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User user = userService.findById(orderDto.getUser().getId());
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        orderDto.setUser(user);

        if (orderDto.getType()==null || OrderType.contains(orderDto.type())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (orderDto.type()==OrderType.DELIVERY && orderDto.getAddress().getId()==null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            Address address = addressService.findById(orderDto.getAddress().getId());
            if (address == null) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            orderDto.setAddress(address);
        }


        List<Long> foodIds = sortedFoods.stream()
                .map(Food::getId)
                .toList();
        Map<Long, Food> foods = foodService.findActiveFoods(foodIds).stream().map(food -> Map.entry(food.getId(), food))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        long foodPrice = 0L; /*sortedFoods.stream()
                .map(Food::getPrice)
                .reduce(0L, Long::sum);*/

        for (CartItem cartItem : orderDto.getFoods()) {
            Food food = foods.get(cartItem.getFood().getId());
            foodPrice += food.getPrice() * cartItem.getCount();
            cartItem.setFood(food);
        }


        Order order = orderRepo.save(OrderMapper.toEntity(orderDto));


        OrderPrice orderPrice = order.getPrice();
        orderPrice.setOrder(order);
        orderPrice.setFoodPrice(foodPrice);
        orderPrice.sumPrice(foodPrice);

        OrderAddress orderAddress = order.getAddress();
        orderAddress.setOrder(order);

        order.setAddress(orderAddressRepo.save(orderAddress));
        order.setPrice(orderPriceRepo.save(orderPrice));

        orderRepo.save(order);

        return new ResponseEntity<>(OrderMapper.toDto(order), HttpStatus.OK);

    }

    public Order findById(Long id) {
        return orderRepo.findByIdAndDeletedFalse(id);
    }

    public boolean delete(Long id) {
        return orderRepo.delete(id);
    }

    public List<Order> findAll() {
        return orderRepo.findAllByDeletedFalse();
    }

    public List<Order> search(String line) {
        return orderRepo.search(line);
    }

}

