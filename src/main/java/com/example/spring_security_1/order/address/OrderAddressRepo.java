package com.example.spring_security_1.order.address;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderAddressRepo extends JpaRepository<OrderAddress, Long> {
}
