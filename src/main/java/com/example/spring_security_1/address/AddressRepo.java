package com.example.spring_security_1.address;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AddressRepo extends JpaRepository<Address, Long> {

    Address findByIdAndDeletedFalse(Long id);

    List<Address> findAllByDeletedFalse();

    @Query(value = "UPDATE address SET deleted = true WHERE id = ?1", nativeQuery = true)
    boolean delete(Long id);
}
