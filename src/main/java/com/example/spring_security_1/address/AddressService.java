package com.example.spring_security_1.address;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepo addressRepo;

    public Address findById(Long id) {
        return addressRepo.findByIdAndDeletedFalse(id);
    }

    public Address save(Address address) {
        return addressRepo.save(address);
    }

    public boolean delete(Long id) {
        return addressRepo.delete(id);
    }

    public List<Address> findAll() {
        return addressRepo.findAllByDeletedFalse();
    }

    public boolean update(Address address) {
        if (addressRepo.findByIdAndDeletedFalse(address.getId()) != null) {
            addressRepo.save(address);
            return true;
        }
        return false;
    }
}
